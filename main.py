# main.py | A tracker to track phone number location,
# country, carrier, ISP and other important information

import json
# Import tkinter GUI library
import os
import tkinter
from tkinter import *
# Import google maps
import googlemaps
# Import phone numbers
import phonenumbers
# Import carriers from phone numbers
from phonenumbers import carrier
# Import Geocoder from phone numbers
from phonenumbers import geocoder
# Import timezone from phone numbers
from phonenumbers import timezone
# Import TimezoneFinder from timezoneFinder package
from timezonefinder import TimezoneFinder
# Import datetime
from datetime import datetime
# Import python timezone
import pytz
# Import environment package
from dotenv import load_dotenv


# Load environment
load_dotenv(".env")

# Initialize the GUI canvas method
root = Tk()
# Set the window title
root.title("Phone Number Tracker")
# Set the window size
root.geometry("365x584+550+130")
# Set resizable to false ( Do not resize )
root.resizable(False, False)


# Track phone number
def track():
    # Get phone number
    phone_number = entry.get()
    # Validate phone number
    if len(phone_number) < 4:
        return

    # Put the button in loading state
    search.config(text="Loading...", default=tkinter.DISABLED)

    number = phonenumbers.parse(phone_number)
    # country
    locate = geocoder.description_for_number(number, "en")
    country.config(text="Country: " + locate)

    # operator like Idea, airtel, jio and many other
    operator = carrier.name_for_number(number, "en")
    sim.config(text="SIM: " + operator)

    # Phone Timezone
    time = timezone.time_zones_for_number(number)
    zone.config(text="TimeZone: " + time[0])

    # Longitude and Latitude
    gmaps = googlemaps.Client(key=os.getenv('GOOGLE_KEY'))
    location = gmaps.geocode(locate)

    # Check if results are returned and set values, else, set lat and lng to 0
    if len(location) > 0:
        location = location[0]["geometry"]["location"]
    else:
        location = dict()
        location["lat"] = 0
        location["lng"] = 0

    # Get values
    lng = location["lng"]
    lat = location["lat"]
    # Set values in view
    longitude.config(text="Longitude: " + lng.__str__())
    latitude.config(text="Latitude: " + lat.__str__())

    # time showing in phone
    obj = TimezoneFinder()
    result = obj.timezone_at(lng=lng, lat=lat)
    home = pytz.timezone(result)
    local_time = datetime.now(home)
    current_time = local_time.strftime("%I:%M:%p")
    clock.config(text="Phone Time: " + current_time)

    # Enable the default text for search button
    search.config(text="Search", default=tkinter.NORMAL)


# LOGO
logo = PhotoImage(file="assets/images/logo image.png")
Label(root, image=logo).place(x=240, y=70)

Heading = Label(root, text="TRACK NUMBER", font=("arial", 15, "bold"))
Heading.place(x=90, y=110)

# ENTRY
Entry_back = PhotoImage(file="assets/images/search png.png")
Label(root, image=Entry_back).place(x=20, y=190)

entry = StringVar()
enter_number = Entry(root, textvariable=entry, highlightbackground="#fff", highlightcolor="#fff", width=15, bd=0,
                     font=("arial", 30), justify="center")
enter_number.place(x=53, y=218)

# BUTTON
Search_image = PhotoImage(file="assets/images/search-icon.png")
search = Button(root, text="Search", image=Search_image, compound=LEFT, justify=tkinter.CENTER, borderwidth=0, bd=0,
                pady=0, padx=0, font=("Helvetica", 20, "bold"), width=300, bg="#ffffff", height=35, command=track)
search.place(x=35, y=300)

# BOTTOM BOX
Box = PhotoImage(file="assets/images/bottom png.png")
Label(root, image=Box).place(x=-2, y=355)

country = Label(root, text="Country:", bg="#57adff", fg="black", font=("arial", 10, "bold"))
country.place(x=50, y=400)

sim = Label(root, text="SIM:", bg="#57adff", fg="black", font=("arial", 10, "bold"))
sim.place(x=200, y=400)

zone = Label(root, text="TimeZone:", bg="#57adff", fg="black", font=("arial", 10, "bold"))
zone.place(x=50, y=450)

clock = Label(root, text="Phone Time:", bg="#57adff", fg="black", font=("arial", 10, "bold"))
clock.place(x=200, y=450)

longitude = Label(root, text="Longitude:", bg="#57adff", fg="black", font=("arial", 10, "bold"))
longitude.place(x=50, y=500)

latitude = Label(root, text="Latitude:", bg="#57adff", fg="black", font=("arial", 10, "bold"))
latitude.place(x=200, y=500)

# Keep window
root.mainloop()
